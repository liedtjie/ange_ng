import { AngeNgPage } from './app.po';

describe('ange-ng App', () => {
  let page: AngeNgPage;

  beforeEach(() => {
    page = new AngeNgPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
