import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';
import {BlogComponent} from './blog/blog.component';
import {HomeComponent} from './home/home.component';
import {AboutComponent} from './about/about.component';
import {ContactUsComponent} from './contact-us/contact-us.component';
import {TheTeamComponent} from './the-team/the-team.component';
import {NavigationComponent} from './navigation/navigation.component';
import {routing} from './app.routing';


@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    HomeComponent,
    AboutComponent,
    BlogComponent,
    TheTeamComponent,
    ContactUsComponent,

  ],
  imports: [
    routing,
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [],
  bootstrap: [
  AppComponent,
  HomeComponent,
  AboutComponent,
  BlogComponent,
  TheTeamComponent,
  ContactUsComponent,
  
  ]
})


@NgModule({
  imports: [
    CommonModule,
    
  ],
  declarations: []
})
export class AppModule {
}
