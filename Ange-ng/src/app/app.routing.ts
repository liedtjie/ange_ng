import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {AboutComponent} from './about/about.component';
import {HomeComponent} from './home/home.component';
import {BlogComponent} from './blog/blog.component';
import {TheTeamComponent} from './the-team/the-team.component';
import {ContactUsComponent} from './contact-us/contact-us.component';


const
  ROUTES: Routes = [
    {path: 'home', component: HomeComponent},
    {path: 'about', component: AboutComponent},
    {path: 'blog', component: BlogComponent},
    {path: 'theteam', component: TheTeamComponent},
    {path: 'contactus', component: ContactUsComponent}
    
  ];

export const routing = RouterModule.forRoot(ROUTES);

@NgModule({
  imports: [
    RouterModule.forRoot(ROUTES)
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AppRoutingModule {
}
